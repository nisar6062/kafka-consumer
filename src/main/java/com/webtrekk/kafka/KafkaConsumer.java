package com.webtrekk.kafka;

import java.util.Base64;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.webtrekk.model.Email;
import com.webtrekk.util.ConsumerCreator;
import com.webtrekk.util.EmailSender;
import com.webtrekk.util.PropertyUtil;

/**
 * @author Nisar Adappadathil Kafka Consumer Application which polls messages
 *         from kafka topic & sends the e-mails.
 */
public class KafkaConsumer {
	private static final Logger LOGGER = Logger.getLogger(KafkaConsumer.class);
	private PropertyUtil propertyUtil;
	private int maxNoOfMsgFound;

	public KafkaConsumer() {
		propertyUtil = new PropertyUtil();
		maxNoOfMsgFound = Integer.parseInt(propertyUtil.getProperty("kafka.maxNoOfMsgFound"));
	}

	public static void main(String[] args) {
		new KafkaConsumer().runConsumer();
	}

	public void runConsumer() {
		LOGGER.info("Consumer Started");
		Consumer<Long, String> consumer = ConsumerCreator.getInstance().createConsumer();
		int noMessageFound = 0;
		while (true) {
			ConsumerRecords<Long, String> consumerRecords = consumer.poll(1000);
			// 1000 is the time in milliseconds consumer will wait if no record is found at broker.
			if (consumerRecords.count() == 0) {
				noMessageFound++;
				if (noMessageFound > maxNoOfMsgFound) {
					// If no message found count is reached to threshold exit loop.
					LOGGER.info("No Record Found");
					break;
				}
				else
					continue;
			}
			// print each record.
			consumerRecords.forEach(record -> {
				LOGGER.info("Record Key: {}" + record.key());
				LOGGER.info("Record value: " + record.value());
				LOGGER.info("Record partition: " + record.partition());
				LOGGER.info("Record offset: " + record.offset());

				ObjectMapper objMap = new ObjectMapper();
				try {
					Email email = objMap.readValue(new String(Base64.getDecoder().decode(record.value())), Email.class);
					LOGGER.info("Email: " + email);
					EmailSender.sendEmail(email);
				} catch (Exception e) {
					e.printStackTrace();
					LOGGER.error(e.getMessage(), e);
				}
			});
			// commits the offset of record to broker.
			consumer.commitAsync();
		}
		consumer.close();
	}
}
