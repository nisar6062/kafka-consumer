package com.webtrekk.util;

import java.io.FileOutputStream;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Date;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMessage.RecipientType;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.webtrekk.model.Email;

/**
 * @author Nisar Adappadathil This class is for sending emails
 */
public class EmailSender {
	private static final Logger LOGGER = Logger.getLogger(EmailSender.class);
	private static final String DEFAULT_FROM_MAIL = "testnisarapp@gmail.com";
	private static final String DEFAULT_FROM_MAIL_NAME = "Nisar Adappadathil";

	public static void sendEmail(Email email) throws Exception {
		LOGGER.info("mail data:" + email);
		String fromMail = DEFAULT_FROM_MAIL;
		if (email.getFromMail() != null) {
			fromMail = email.getFromMail();
		}
		Session session = getSession(fromMail, email.getPassword());
		int maxRetry = Integer.parseInt(new PropertyUtil().getProperty("max.smtp.retry"));
		sendEmail(session, email, maxRetry);
	}

	private static Session getSession(String fromEmail, String password) {
		Properties props = new Properties();
		props.put("mail.smtp.host", "smtp.gmail.com"); // SMTP Host
		props.put("mail.smtp.socketFactory.port", "465"); // SSL Port
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory"); // SSL Factory Class
		props.put("mail.smtp.ssl.checkserveridentity", true);
		props.put("mail.smtp.auth", "true"); // Enabling SMTP Authentication
		props.put("mail.smtp.port", "465"); // SMTP Port

		LOGGER.info("kafka props:" + props);
		Authenticator auth = new Authenticator() {
			// override the getPasswordAuthentication method
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		};
		return Session.getDefaultInstance(props, auth);
	}

	/**
	 * Utility method to send simple HTML email
	 * 
	 * @param session
	 * @param toEmail
	 * @param subject
	 * @param body
	 * @throws Exception
	 */
	private static void sendEmail(Session session, Email email, int maxRetry) throws Exception {
		try {
			MimeMessage msg = new MimeMessage(session);
			// set message headers
			msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
			msg.addHeader("format", "flowed");
			msg.addHeader("Content-Transfer-Encoding", "8bit");
			msg.setFrom(new InternetAddress(email.getFromMail(), DEFAULT_FROM_MAIL_NAME));
			if (email.getCcList() != null) {
				for (String cc : email.getCcList()) {
					msg.addRecipient(RecipientType.CC, new InternetAddress(cc));
				}
			}
			if (email.getBccList() != null) {
				for (String bcc : email.getBccList()) {
					msg.addRecipient(RecipientType.BCC, new InternetAddress(bcc));
				}
			}
			msg.setReplyTo(InternetAddress.parse(email.getFromMail(), false));
			msg.setSubject(email.getSubject(), "UTF-8");
			msg.setText(email.getBody());
			msg.setSentDate(new Date());

			// Set Attachment
			BodyPart messageBodyPart = new MimeBodyPart();
			messageBodyPart.setText(email.getBody());
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();
			// Adding attachment
			String fileName = downloadFile(email.getAttachmentURL());
			LOGGER.info("Attachment fileName:" + fileName);
			DataSource source = new FileDataSource(fileName);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(fileName);
			multipart.addBodyPart(messageBodyPart);
			msg.setContent(multipart);

			msg.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email.getToMail(), false));
			Transport.send(msg);
			LOGGER.info("Email Send to :" + email.getToMail());
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
			if (maxRetry > 0) {
				sendEmail(session, email, maxRetry - 1);
			}
			throw e;
		}
	}

	public static String downloadFile(String attachmentURL) {
		try {
			URL website = new URL(attachmentURL);
			ReadableByteChannel rbc = Channels.newChannel(website.openStream());
			String file = attachmentURL.substring(attachmentURL.lastIndexOf('/') + 1);
			FileOutputStream fos = new FileOutputStream(file);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
			return file;
		} catch (Exception e) {
			LOGGER.error(e.getMessage(), e);
		}
		return null;
	}
}
