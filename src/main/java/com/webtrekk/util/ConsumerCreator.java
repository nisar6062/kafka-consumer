package com.webtrekk.util;

import java.util.Collections;
import java.util.Properties;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
/**
 * @author Nisar Adappadathil
 * This creates the Kafka Consumer
 */
public class ConsumerCreator {

	private String kafkaBrokers;
	private String topic;
	private String groupId;
	private String offset;
	private int maxPollRecords;

	private PropertyUtil propertyUtil;

	private ConsumerCreator() {
		propertyUtil = new PropertyUtil();
		kafkaBrokers = propertyUtil.getProperty("kafka.brokers");
		topic = propertyUtil.getProperty("kafka.topic");
		groupId = propertyUtil.getProperty("kafka.groupId");
		offset = propertyUtil.getProperty("kafka.offset");
		maxPollRecords = Integer.parseInt(propertyUtil.getProperty("kafka.maxPollRecords"));

	}

	private static volatile ConsumerCreator instance;

	public static ConsumerCreator getInstance() {
		if (instance == null) {
			synchronized (ConsumerCreator.class) {
				if (instance == null) {
					instance = new ConsumerCreator();
				}
			}
		}
		return instance;
	}

	public Consumer<Long, String> createConsumer() {
		Properties props = new Properties();
		props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBrokers);
		props.put(ConsumerConfig.GROUP_ID_CONFIG, groupId);
		props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
		props.put(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, maxPollRecords);
		props.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
		props.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, offset);
		Consumer<Long, String> consumer = new KafkaConsumer<>(props);
		consumer.subscribe(Collections.singletonList(topic));
		return consumer;
	}
}
