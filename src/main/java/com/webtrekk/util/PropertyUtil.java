package com.webtrekk.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class PropertyUtil {
	private static final Logger LOGGER = Logger.getLogger(PropertyUtil.class);
	private final String propertyFileName = "kafka-config.properties";
	private Properties prop;

	public PropertyUtil() {
		prop = initalizeProperties();
	}

	public String getProperty(String key) {
		return prop.getProperty(key);
	}

	public static String getLanguageCode(String input) {
		Properties prop = new PropertyUtil().initalizeProperties();
		return prop.getProperty(input.toLowerCase());
	}

	private Properties initalizeProperties() {
		InputStream inputStream = null;
		try {
			Properties prop = new Properties();

			inputStream = getClass().getClassLoader().getResourceAsStream(propertyFileName);

			if (inputStream != null) {
				prop.load(inputStream);
			} else {
				throw new FileNotFoundException("property file '" + propertyFileName + "' not found in the classpath");
			}
			return prop;
		} catch (IOException e) {
			LOGGER.error(e.getMessage(), e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					LOGGER.error(e.getMessage(), e);
				}
			}
		}
		return null;
	}
}
