package com.webtrekk.model;

import java.util.List;

/**
 * @author Nisar Adappadathil 
 * Email POJO
 */
public class Email {
	private Long id;
	private String password;
	private String toMail;
	private String fromMail;
	private String subject;
	private String body;
	private List<String> ccList;
	private List<String> bccList;
	private String attachmentURL;

	public Email() {

	}

	public Email(EmailBuilder builder) {
		super();
		this.password = builder.password;
		this.toMail = builder.toMail;
		this.fromMail = builder.fromMail;
		this.subject = builder.subject;
		this.body = builder.body;
		this.ccList = builder.ccList;
		this.bccList = builder.bccList;
		this.attachmentURL = builder.attachmentURL;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToMail() {
		return toMail;
	}

	public void setToMail(String toMail) {
		this.toMail = toMail;
	}

	public String getFromMail() {
		return fromMail;
	}

	public void setFromMail(String fromMail) {
		this.fromMail = fromMail;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getBody() {
		return body;
	}

	public void setBody(String body) {
		this.body = body;
	}

	public List<String> getCcList() {
		return ccList;
	}

	public void setCcList(List<String> ccList) {
		this.ccList = ccList;
	}

	public List<String> getBccList() {
		return bccList;
	}

	public void setBccList(List<String> bccList) {
		this.bccList = bccList;
	}

	public String getAttachmentURL() {
		return attachmentURL;
	}

	public void setAttachmentURL(String attachmentURL) {
		this.attachmentURL = attachmentURL;
	}

	public static class EmailBuilder {
		private String password;
		private String toMail;
		private String fromMail;
		private String subject;
		private String body;
		private List<String> ccList;
		private List<String> bccList;
		private String attachmentURL;

		public EmailBuilder password(String password) {
			this.password = password;
			return this;
		}

		public EmailBuilder toMail(String toMail) {
			this.toMail = toMail;
			return this;
		}

		public EmailBuilder fromMail(String fromMail) {
			this.fromMail = fromMail;
			return this;
		}

		public EmailBuilder subject(String subject) {
			this.subject = subject;
			return this;
		}

		public EmailBuilder body(String body) {
			this.body = body;
			return this;
		}

		public EmailBuilder ccList(List<String> ccList) {
			this.ccList = ccList;
			return this;
		}

		public EmailBuilder bccList(List<String> bccList) {
			this.bccList = bccList;
			return this;
		}

		public EmailBuilder attachmentURL(String attachmentURL) {
			this.attachmentURL = attachmentURL;
			return this;
		}

		public Email build() {
			return new Email(this);
		}
	}

	@Override
	public String toString() {
		return "Email [id=" + id + ", password=" + password + ", toMail=" + toMail + ", fromMail=" + fromMail
				+ ", subject=" + subject + ", body=" + body + ", ccList=" + ccList + ", bccList=" + bccList
				+ ", attachmentURL=" + attachmentURL + "]";
	}

}
